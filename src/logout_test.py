from webdriver_manager.chrome import ChromeDriverManager 
from selenium import webdriver 
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time
import unittest

class TestLogoutPage(unittest.TestCase):

    browser = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install())) 
    url="https://www.saucedemo.com/"
    id_username="user-name"
    id_password="password"
    id_button="login-button"

    def test_logout(self):
        self.browser.get(self.url)

        campo_login = self.browser.find_element(By.ID, self.id_username)
        campo_login.send_keys('standard_user')
        campo_password = self.browser.find_element(By.ID, self.id_password)
        campo_password.send_keys('secret_sauce') 
        botao_login = self.browser.find_element(By.ID, self.id_button)
        time.sleep(2)
        botao_login.click()
        time.sleep(2)
        self.browser.find_element(By.ID,'react-burger-menu-btn').click()
        time.sleep(2)
        self.browser.find_element(By.ID,'logout_sidebar_link').click()
        time.sleep(2)
        # assert if login button is present after logout
        login_button = self.browser.find_element(By.NAME,"login-button")
        time.sleep(2)
        login_text = login_button.get_attribute("value")
        assert login_text == 'Login'
        print("SUCCESS # LOGGED OUT")
        print(login_text)
        
    

if __name__ == '__main__':
        unittest.main()