from webdriver_manager.chrome import ChromeDriverManager 
from selenium import webdriver 
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time
import unittest

class TestCartPage(unittest.TestCase):

    #browser = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install())) 
    url="https://www.saucedemo.com/"
    id_username="user-name"
    id_password="password"
    id_button="login-button"
    
    
    def login(self):        
        self.browser = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install())) 
        self.browser.get(self.url)
        campo_login = self.browser.find_element(By.ID, self.id_username)
        campo_login.send_keys('standard_user')
        campo_password = self.browser.find_element(By.ID, self.id_password)
        campo_password.send_keys('secret_sauce') 
        botao_login = self.browser.find_element(By.ID, self.id_button)
        botao_login.click()                
        time.sleep(2)

    def test_adicionar_botao(self):
        self.login()
        self.browser.find_element(By.ID,'add-to-cart-sauce-labs-backpack').click()
        time.sleep(2)
        remover=self.browser.find_element(By.ID,'remove-sauce-labs-backpack').text
        time.sleep(2)
        self.browser.close  
        assert remover == 'Remove'
        print("SUCCESS # Adicionado com sucesso")
     
    def test_adicionar_multiplos(self):       
        
        self.login()        
        self.browser.find_element(By.ID,'add-to-cart-sauce-labs-backpack').click()            
        time.sleep(2)
        self.browser.find_element(By.ID,'add-to-cart-sauce-labs-bike-light').click()              
        time.sleep(2)
        self.browser.find_element(By.ID,'add-to-cart-sauce-labs-bolt-t-shirt').click()              
        time.sleep(2)
        valor=self.browser.find_element(By.XPATH,'//*[@id="shopping_cart_container"]/a/span').text       
        assert valor == '3'
        print("SUCCESS # TESTE DE MULTIPLOS OK")        
    
    def test_remover_botao(self):
        self.login()
        self.browser.find_element(By.ID,'add-to-cart-sauce-labs-backpack').click()
        time.sleep(2)
        remover=self.browser.find_element(By.ID,'remove-sauce-labs-backpack').text
        time.sleep(2)  
        assert remover == 'Remove'
        print("SUCCESS # Removido com sucesso")

    def test_open_cart(self):
        self.login()
        self.browser.find_element(By.XPATH,'//*[@id="shopping_cart_container"]/a').click()
        time.sleep(2)
        remover=self.browser.find_element(By.XPATH,'//*[@id="header_container"]/div[2]/span').text
        time.sleep(2)  
        assert remover == 'Your Cart'
        print("SUCCESS # CARRINHO OK")


if __name__ == '__main__':
        unittest.main()