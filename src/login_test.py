from webdriver_manager.chrome import ChromeDriverManager 
from selenium import webdriver 
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

import unittest

class TestLoginPage(unittest.TestCase):

    browser = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install())) 
    url="https://www.saucedemo.com/"
    id_username="user-name"
    id_password="password"
    id_button="login-button"

    def test_valid_login(self):
        self.browser.get(self.url)
        campo_login = self.browser.find_element(By.ID, self.id_username)
        campo_login.send_keys('standard_user')
        campo_password = self.browser.find_element(By.ID, self.id_password)
        campo_password.send_keys('secret_sauce')        
        botao_login = self.browser.find_element(By.ID, self.id_button)
        time.sleep(1)
        botao_login.click()
        time.sleep(1)        
        label_titulo = self.browser.find_element(By.CLASS_NAME,"title").text
        assert label_titulo=='Products'
        print("\nSUCCESS # USERNAME (standard_user) e SENHA (secret_sauce)")
        
    def test_locked_login(self):
        self.browser.get(self.url)
        campo_login = self.browser.find_element(By.ID, self.id_username)
        campo_login.send_keys('locked_out_user')
        campo_password = self.browser.find_element(By.ID, self.id_password)
        campo_password.send_keys('secret_sauce')        
        botao_login = self.browser.find_element(By.ID, self.id_button)
        time.sleep(1)
        botao_login.click()
        time.sleep(1)        
        lock = self.browser.find_element(By.XPATH,'//div[@id="login_button_container"]/div/form/div[3]/h3').text
        assert lock == 'Epic sadface: Sorry, this user has been locked out.'
        print("\nSUCCESS # USER LOCKED OUT (locked_out_user) e SENHA (secret_sauce)")
        
        #self.assertEqual("Epic sadface: Username and password do not match any user in this service",mensagem)

    def test_invalid_login(self):
        self.browser.get(self.url)
        campo_login = self.browser.find_element(By.ID, self.id_username)
        campo_login.send_keys('standard_user_fail')
        campo_password = self.browser.find_element(By.ID, self.id_password)
        campo_password.send_keys('secret_sauce')        
        botao_login = self.browser.find_element(By.ID, self.id_button)
        time.sleep(1)
        botao_login.click()
        time.sleep(1)        
        invalid = self.browser.find_element(By.XPATH,'//div[@id="login_button_container"]/div/form/div[3]/h3').text
        assert invalid == 'Epic sadface: Username and password do not match any user in this service'
        print("\nSUCCESS # INVALID LOGIN (standard_user_fail) e SENHA (secret_sauce)")

    def test_nouser_login(self):
        self.browser.get(self.url)
        campo_login = self.browser.find_element(By.ID, self.id_username)
        campo_login.send_keys('')
        campo_password = self.browser.find_element(By.ID, self.id_password)
        campo_password.send_keys('secret_sauce')        
        botao_login = self.browser.find_element(By.ID, self.id_button)
        time.sleep(1)
        botao_login.click()
        time.sleep(1)        
        nouser = self.browser.find_element(By.XPATH,'//div[@id="login_button_container"]/div/form/div[3]/h3').text
        assert nouser == 'Epic sadface: Username is required'
        print("\nSUCCESS # SEM USUARIO LOGIN ( ) e SENHA (secret_sauce)")
    
    def test_nopassword_login(self):
        self.browser.get(self.url)
        campo_login = self.browser.find_element(By.ID, self.id_username)
        campo_login.send_keys('standard_user')
        campo_password = self.browser.find_element(By.ID, self.id_password)
        campo_password.send_keys('')        
        botao_login = self.browser.find_element(By.ID, self.id_button)
        time.sleep(1)
        botao_login.click()
        time.sleep(1)        
        nopass= self.browser.find_element(By.XPATH,'//div[@id="login_button_container"]/div/form/div[3]/h3').text
        assert nopass == 'Epic sadface: Password is required'
        print("\nSUCCESS # SEM SENHA LOGIN (standard_user_fail) e SENHA ( )") 

if __name__ == '__main__':
        unittest.main()